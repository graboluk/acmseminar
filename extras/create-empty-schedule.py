import sys
#dirty hack to make sure we can find mkpage_common.py
sys.path.extend(["..","../..","../../..","../../../.."])
import mkpage_common
import yaml
import datetime 

# argument of this script is a jinja template for the schedule,
# so to generate a new empty schedule typically run
# python3 <this_file.py> schedule.jinja > schedula.yaml


# 0 is monday, 4 is friday
academic_year="2016-17"
day = 0
start=list(range(3))
end=list(range(3))
start[0] = (2017,10,16)
end[0] = (2017,12,7)
start[1] = (2018,1,12)
end[1] = (2018,3,23)
start[2] = (2018,4,20)
end[2] = (2018,6,29)

#There should be no need to edit beyound this point

dates = list(range(3))
for t in range(3):
    dates[t]=[]
    sdate = datetime.date(*start[t])
    tdate = datetime.date(*end[t])
    oneday = datetime.timedelta(days=1)
    if sdate > tdate:
        print("Start date of each term must be before the end date. (Check term " + str(t) +")")
        sys.exit(1)

    d=sdate
    while d<=tdate:
        if d.weekday() == day:
            dates[t].append(str(d))#.strftime("%b %d"))
        d +=oneday

templateVars={"academic_year":academic_year, "dates":dates}
mkpage_common.render(templateVars)
